package br.com.restproject.service;

import java.util.List;

import javassist.NotFoundException;
import br.com.restproject.beans.Dono;

public interface DonoService {
	Dono insere(Dono dono);
	Dono atualiza(Dono dono);

	boolean verificaExistencia(int id) throws NotFoundException;

	Dono buscaPorId(int id) throws NotFoundException;
	
	List<Dono> buscaPorNome(String nome) throws NotFoundException;
	List<Dono> buscaPorAnoNascimento(int ano) throws NotFoundException;

	List<Dono> retornaTodos() throws NotFoundException;
}
