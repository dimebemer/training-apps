package br.com.restproject.service.impl;

import java.util.List;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.restproject.beans.Cachorro;
import br.com.restproject.repository.CachorroRepository;
import br.com.restproject.service.CachorroService;

@Service
public class CachorroServiceImpl implements CachorroService {

	@Autowired
	private CachorroRepository repository;

	@Override
	public Cachorro insere(Cachorro cachorro) {
		repository.save(cachorro);
		return repository.findById(cachorro.getId());
	}

	@Override
	public Cachorro atualiza(Cachorro cachorro) {
		repository.save(cachorro);
		return repository.findById(cachorro.getId());
	}

	@Override
	public Cachorro buscaPorId(int id) throws NotFoundException {
		Cachorro cachorro = repository.findById(id);

		if(cachorro == null) {
			throw new NotFoundException("Cachorro não encontrado!");
		}

		return cachorro;
	}

	@Override
	public List<Cachorro> buscaPorNome(String nome) throws NotFoundException {
		return validaListaCachorros(repository.findByNome(nome), 
				"Nenhum cachorro encontrado com este nome!");
	}
	
	@Override
	public List<Cachorro> buscaPorRaca(String raca) throws NotFoundException {
		return validaListaCachorros(repository.findByRaca(raca), 
				"Nenhum cachorro encontrado com esta raça!");
	}

	@Override
	public List<Cachorro> buscaPorIdDono(int idDono) throws NotFoundException {
		return validaListaCachorros(repository.findByDonoId(idDono),
				"Nenhum cachorro encontrado para este dono.");
	}
	
	@Override
	public List<Cachorro> retornaTodos() throws NotFoundException {
		return validaListaCachorros(repository.findAll(), 
				"Nenhum cachorro registrado.");
	}
	
	private List<Cachorro> validaListaCachorros(List<Cachorro> cachorros, String mensagem) throws NotFoundException {
		if(cachorros == null || cachorros.isEmpty()) {
			throw new NotFoundException(mensagem);
		}
		
		return cachorros;
	}
}
