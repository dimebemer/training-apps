package br.com.restproject.service;

import java.util.List;

import javassist.NotFoundException;
import br.com.restproject.beans.Cachorro;

public interface CachorroService {
	Cachorro insere(Cachorro cachorro);
	Cachorro atualiza(Cachorro cachorro);

	Cachorro buscaPorId(int id) throws NotFoundException;
	List<Cachorro> buscaPorNome(String nome) throws NotFoundException;
	List<Cachorro> buscaPorRaca(String raca) throws NotFoundException;
	List<Cachorro> buscaPorIdDono(int idDono) throws NotFoundException;

	List<Cachorro> retornaTodos() throws NotFoundException;
}
