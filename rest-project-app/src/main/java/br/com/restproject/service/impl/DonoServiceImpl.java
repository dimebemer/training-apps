package br.com.restproject.service.impl;

import java.util.List;

import javassist.NotFoundException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.restproject.beans.Dono;
import br.com.restproject.repository.DonoRepository;
import br.com.restproject.service.DonoService;

@Service
public class DonoServiceImpl implements DonoService {

	private static final String DONO_NAO_ENCONTRADO = "Dono não encontrado!";
	
	@Autowired
	private DonoRepository repository;

	@Override
	public Dono insere(Dono dono) {
		repository.save(dono);
		return repository.findById(dono.getId());
	}

	@Override
	public Dono atualiza(Dono dono) {
		repository.save(dono);
		return repository.findById(dono.getId());
	}

	@Override
	public Dono buscaPorId(int id) throws NotFoundException {
		return validaDono(repository.findById(id));
	}

	@Override
	public List<Dono> buscaPorNome(String nome) throws NotFoundException {
		return validaListaDonos(repository.findByNome(nome),
				"Nenhum dono encontrado com este nome!");
	}

	@Override
	public List<Dono> buscaPorAnoNascimento(int ano) throws NotFoundException {
		List<Dono> donos = repository.findByDataNascimentoBetween(
				new DateTime(ano + "01-01"), 
				new DateTime(ano + "31-12")
		);
		
		return validaListaDonos(donos, "Nenhum dono nasceu no ano informado!");
	}

	@Override
	public List<Dono> retornaTodos() throws NotFoundException {
		return validaListaDonos(repository.findAll(), "Nenhum dono registrado.");
	}

	@Override
	public boolean verificaExistencia(int id) throws NotFoundException {
		boolean existe = repository.exists(id);
		
		if(!existe) {
			throw new NotFoundException(DONO_NAO_ENCONTRADO);
		}
		
		return existe;
	}
	
	private Dono validaDono(Dono dono) throws NotFoundException {
		if (dono == null) {
			throw new NotFoundException(DONO_NAO_ENCONTRADO);
		}

		return dono;
	}

	private List<Dono> validaListaDonos(List<Dono> donos, String mensagem) throws NotFoundException {
		if (donos == null || donos.isEmpty()) {
			throw new NotFoundException(mensagem);
		}

		return donos;
	}


}
