package br.com.restproject.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.restproject.beans.Dono;

@RepositoryRestResource
public interface DonoRepository extends Repository<Dono, Integer>{
	
	public List<Dono> findAll();
	
	@EntityGraph(value = "Dono.detalhes", type = EntityGraphType.LOAD)
	public Dono findById(int id);
	
	public Dono save(Dono dono);
	
	@EntityGraph(value = "Dono.detalhes", type = EntityGraphType.LOAD)
	public List<Dono> findByNome(String nome);
	
	@EntityGraph(value = "Dono.detalhes", type = EntityGraphType.LOAD)
	public List<Dono> findByDataNascimentoBetween(DateTime inicio, DateTime fim);

	public boolean exists(Integer id);
	
}
