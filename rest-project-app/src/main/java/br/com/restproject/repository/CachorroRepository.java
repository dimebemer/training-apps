package br.com.restproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.restproject.beans.Cachorro;

@RepositoryRestResource
public interface CachorroRepository extends Repository<Cachorro, Integer>{
	// http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html 
	// http://projects.spring.io/spring-data/
	// http://docs.spring.io/spring-data/jdbc/docs/1.2.1.RELEASE/reference/html/
	// http://docs.spring.io/spring-data/rest/docs/current/reference/html/
	// http://docs.spring.io/spring-data/rest/docs/2.1.4.RELEASE/reference/html/repository-resources.html
	// http://docs.spring.io/spring-hateoas/docs/current/reference/html/
	// https://www.safaribooksonline.com/blog/2013/09/30/rest-hypermedia/
	// http://spring.io/guides/tutorials/bookmarks/
	// http://mark-kirby.co.uk/2013/creating-a-true-rest-api/
	// http://www.baeldung.com/2011/10/25/building-a-restful-web-service-with-spring-3-1-and-java-based-configuration-part-2/
	
	public List<Cachorro> findAll();
	
	public Cachorro save(Cachorro cachorro);
	
	@EntityGraph(value = "Cachorro.detalhes", type = EntityGraphType.LOAD)
	public Cachorro findById(int id);

	@EntityGraph(value = "Cachorro.detalhes", type = EntityGraphType.LOAD)
	public List<Cachorro> findByNome(String nome);

	@EntityGraph(value = "Cachorro.detalhes", type = EntityGraphType.LOAD)
	public List<Cachorro> findByRaca(String raca);

	public List<Cachorro> findByDonoId(int id);

}
