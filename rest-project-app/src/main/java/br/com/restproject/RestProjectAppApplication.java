package br.com.restproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

@SpringBootApplication
public class RestProjectAppApplication {
	
	@Bean
	public Hibernate4Module hibernate4Module() {
	    Hibernate4Module module = new Hibernate4Module();
	    return module;
	}

    public static void main(String[] args) {
        SpringApplication.run(RestProjectAppApplication.class, args);
    }
    
}
