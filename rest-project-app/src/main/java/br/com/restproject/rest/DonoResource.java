package br.com.restproject.rest;

import java.util.List;

import javassist.NotFoundException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.restproject.beans.Cachorro;
import br.com.restproject.beans.Dono;
import br.com.restproject.beans.view.Views;
import br.com.restproject.service.CachorroService;
import br.com.restproject.service.DonoService;

@RestController
@RequestMapping("/donos")
public class DonoResource extends AbstractResource{

	@Autowired
	private DonoService service;

	@Autowired
	private CachorroService cachorroService;

	@JsonView(Views.Summary.class)
	@RequestMapping(value="/", method=RequestMethod.GET)
	public List<Dono> lista() throws NotFoundException {
		return service.retornaTodos();
	}
	
	@RequestMapping(value="/busca", method=RequestMethod.GET)
	public List<Dono> busca(
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) Integer anoNascimento) throws Exception {
		if (nome != null) {
			return service.buscaPorNome(nome);
		}

		if (anoNascimento != null) {
			return service.buscaPorAnoNascimento(anoNascimento);
		}

		throw new Exception("Parâmetros inválidos!");
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Dono retornaPorId(@PathVariable int id) throws NotFoundException {
		return service.buscaPorId(id);
	}

	@JsonView(Views.Summary.class)
	@RequestMapping(value="/{id}/cachorros", method=RequestMethod.GET)
	public List<Cachorro> retornaListaCachorros(@PathVariable int id) throws NotFoundException {
		service.verificaExistencia(id);
		return cachorroService.buscaPorIdDono(id);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value="/cadastrar", method=RequestMethod.POST, params="!id")
	public Dono cadastra(@Valid Dono dono, BindingResult result) throws BindException {
		validaModel(result);
		return service.insere(dono);
	}

	@RequestMapping(value="/atualizar/{id}", method=RequestMethod.PUT)
	public Dono atualiza(@Valid Dono dono, BindingResult result) throws BindException {
		validaModel(result);
		return service.atualiza(dono);
	}

}
