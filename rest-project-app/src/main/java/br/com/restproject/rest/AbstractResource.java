package br.com.restproject.rest;

import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

public abstract class AbstractResource {
	
	protected void validaModel(BindingResult result) throws BindException {
		if(result.hasErrors()){
			throw new BindException(result);
		}
	}
	
}
