package br.com.restproject.rest;

import java.util.List;

import javassist.NotFoundException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.restproject.beans.Cachorro;
import br.com.restproject.beans.view.Views;
import br.com.restproject.service.CachorroService;

import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("/cachorros")
public class CachorroResource extends AbstractResource{

	@Autowired
	private CachorroService service;

	@JsonView(Views.Summary.class)
	@RequestMapping(value="/", method=RequestMethod.GET)
	public List<Cachorro> lista() throws NotFoundException {
		return service.retornaTodos();
	}

	@RequestMapping(value="/busca", method=RequestMethod.GET)
	public List<Cachorro> busca(
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) String raca) throws Exception {
		
		if (nome != null) {
			return service.buscaPorNome(nome);
		}
		
		if (raca != null) {
			return service.buscaPorRaca(raca);
		}
		
		throw new Exception("Parâmetros inválidos!");
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Cachorro retornaPorId(@PathVariable int id) throws NotFoundException {
		return service.buscaPorId(id);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value="/cadastrar", method=RequestMethod.POST, params={"!id", "dono.id"})
	public Cachorro cadastra(@Valid Cachorro cachorro, BindingResult result) throws BindException {
		validaModel(result);
		return service.insere(cachorro);
	}

	@RequestMapping(value="/atualizar/{id}", method=RequestMethod.PUT)
	public Cachorro atualiza(@Valid Cachorro cachorro, BindingResult result) throws BindException {
		validaModel(result);
		return service.atualiza(cachorro);
	}
}
