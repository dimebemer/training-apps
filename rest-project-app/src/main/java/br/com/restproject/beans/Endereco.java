package br.com.restproject.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "T_ENDERECO")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Endereco implements Serializable{
	private static final long serialVersionUID = 2405868839734926946L;

	@Id	@GeneratedValue
	@Column(name = "cd_endereco")
	private int id;

	@Column(name = "ds_logradouro", nullable = false)
	private String logradouro;

	@Column(name = "nr_numero")
	private int numero;

	@Column(name = "ds_complemento")
	private String complemento;

	@Column(name = "ds_cidade")
	private String cidade;

	@Column(name = "ds_pais", nullable = false)
	private String pais;

	@OneToMany(mappedBy = "endereco")
	private List<Dono> donos;

	public Endereco() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public List<Dono> getDonos() {
		return donos;
	}

	public void setDonos(List<Dono> donos) {
		this.donos = donos;
	}

}
