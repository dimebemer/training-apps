package br.com.restproject.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.restproject.beans.view.Views;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="T_CACHORRO")
@NamedEntityGraph(name = "Cachorro.detalhes",
	attributeNodes = @NamedAttributeNode("dono"))
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Cachorro implements Serializable {

	private static final long serialVersionUID = -1263566222721090318L;

	@Id @GeneratedValue
	@Column(name="cd_cachorro")
	@JsonView(Views.Summary.class)
	private int id;

	@NotNull
	@Column(name="nm_nome", nullable=false)
	@JsonView(Views.Summary.class)
	private String nome;

	@Column(name="ds_raca")
	@JsonView(Views.Summary.class)
	private String raca = "SRD";

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cd_dono")
	private Dono dono;

	public Cachorro() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public Dono getDono() {
		return dono;
	}

	public void setDono(Dono dono) {
		this.dono = dono;
	}

}
