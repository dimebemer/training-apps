package br.com.restproject.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.restproject.beans.view.Views;

@Entity
@Table(name = "T_DONO")
@NamedEntityGraph(name = "Dono.detalhes",
	attributeNodes = @NamedAttributeNode("endereco"))
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Dono implements Serializable{

	private static final long serialVersionUID = 500125646323734485L;

	@Id	@GeneratedValue
	@Column(name = "cd_dono")
	@JsonView(Views.Summary.class)
	private int id;

	@NotNull
	@Column(name = "nm_nome", nullable=false)
	@JsonView(Views.Summary.class)
	private String nome;

	@Column(name = "dt_nascimento")
	@JsonView(Views.Summary.class)
	private DateTime dataNascimento;

	@ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	@JoinColumn(name = "cd_endereco")
	private Endereco endereco;

	@OneToMany(mappedBy="dono")
	@JsonIgnore
	private List<Cachorro> cachorros;

	public Dono() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public DateTime getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(DateTime dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Cachorro> getCachorros() {
		return cachorros;
	}

	public void setCachorros(List<Cachorro> cachorros) {
		this.cachorros = cachorros;
	}

}
