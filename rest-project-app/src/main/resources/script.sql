create table t_endereco (
	cd_endereco INT auto_increment primary key,
    ds_logradouro VARCHAR(255) not null,
    nr_numero INT,
    ds_complemento VARCHAR(255),
    ds_cidade VARCHAR(255),
    ds_pais VARCHAR(255) not null
);

create table t_dono (
	cd_dono INT auto_increment primary key,
    nm_nome VARCHAR(255) not null,
    dt_nascimento datetime,
    cd_endereco INT,
    foreign key (cd_endereco) references t_endereco(cd_endereco)
);

create table t_cachorro (
	cd_cachorro INT auto_increment primary key,
    nm_nome VARCHAR(255) not null,
    ds_raca VARCHAR(255) default 'SRD',
    cd_dono INT,
    foreign key (cd_dono) references t_dono(cd_dono)
);

create or replace view vw_dono_endereco as
	select * 
	from t_dono d
	join t_endereco e using(cd_endereco);